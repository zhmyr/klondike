export let Deck = {
    name: "Deck",
    template: `
    <div v-if="item !== undefined" :id="item.id" class="card" :style="stl">
        <Deck v-if="deck.length > 0" :item="deck[0]" :deck="deck.slice(1)" :ofs="ofset" />
    </div>
    <div v-else class="card"></div> 
    `,
    computed: {
        stl: function () {
            return {
                backgroundImage: "url(" + this.item.img + ")",
                top: this.hold ? 0 : this.ofs + "px"
            };
        },
        ofset: function () {
            return this.ofs == 0 ? 20 : this.ofs;
        }
    },
    props: {
        item: Object,
        deck: Array,
        ofs: Number,
        hold: {
            default: false,
            type: Boolean
        }
    }
};
export let Stack = {
    name: "Stack",
    template: `
    <div class="holder" :ref="rf">
        <div v-for="it in stack" :key="it.id" :data-id="it.id" class="card" :style="{backgroundImage: 'url('+it.img + ')'}" > </div>
    </div>
    `,
    props: {
        stack: Array,
        rf: String
    }
};